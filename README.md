# Unity International LimeSurvey Theme

This theme is based on the theme for "Your COVID-19 Risk" project. That theme, in turn, was based on the Vanilla theme for LimeSurvey 3.x, and was developed by:

Contributors:

- Jan Ehrhardt (Tools for Research): javascript additions
- Tammo ter Hark (Respondage): setup, styling
- Stefan Verweij (Evently): code review, add logo

The original theme is available at https://github.com/tammoterhark/ls3-covid-theme
